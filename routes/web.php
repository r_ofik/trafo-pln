<?php

use App\Http\Controllers\AddressController;
use App\Http\Controllers\GttController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TrafoController;
// use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes([
    'register'=>false
]);

Route::middleware('auth')->group(function () {
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::resource('users', UserController::class)->except(['show']);
    Route::resource('gtts', GttController::class)->except(['show']);
    Route::resource('address', AddressController::class)->except(['show']);
    Route::resource('gardu', TrafoController::class)->except(['edit', 'show']);
    Route::get('gardu/create/{gardu?}', [TrafoController::class, 'create'])->name('gardu.create');
    Route::get('gardu/detail/{id}', [TrafoController::class, 'detail'])->name('gardu.detail');
});