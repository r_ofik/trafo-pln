<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Trafo;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class TrafoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trafos = Address::with('gardu')->get();
        
        return view('gardu.index', compact('trafos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($gardu = null)
    {
        $addresses = Address::latest()->get();
        
        return view('gardu.create', compact('addresses', 'gardu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'address'       => 'required|integer',
            
            'line_ar'       => 'required|integer',
            'line_as'       => 'required|integer',
            'line_at'       => 'required|integer',
            'line_an'       => 'required|integer',

            'line_br'       => 'required|integer',
            'line_bs'       => 'required|integer',
            'line_bt'       => 'required|integer',
            'line_bn'       => 'required|integer',

            'line_cr'       => 'required|integer',
            'line_cs'       => 'required|integer',
            'line_ct'       => 'required|integer',
            'line_cn'       => 'required|integer',

            'line_dr'       => 'required|integer',
            'line_ds'       => 'required|integer',
            'line_dt'       => 'required|integer',
            'line_dn'       => 'required|integer',

            'tegangan_rn'   => 'required|integer',
            'tegangan_sn'   => 'required|integer',
            'tegangan_tn'   => 'required|integer',
        ]);

        DB::beginTransaction();
        try {
            // Check Document
            $document_name = '';
            if ($request->file('document')) {
                $name = time().'-'.date('dmY').'.'.$request->document->clientExtension();
                Storage::disk('public')->put('documents/'.$name, File::get($request->document));
                $document_name = $name;
            }

            // Save to transaction History
            $transaction = Transaction::create([
                'line_ar' => $request->line_ar,
                'line_as' => $request->line_as,
                'line_at' => $request->line_at,
                'line_an' => $request->line_an,
                'line_br' => $request->line_br,
                'line_bs' => $request->line_bs,
                'line_bt' => $request->line_bt,
                'line_bn' => $request->line_bn,
                'line_cr' => $request->line_cr,
                'line_cs' => $request->line_cs,
                'line_ct' => $request->line_ct,
                'line_cn' => $request->line_cn,
                'line_dr' => $request->line_dr,
                'line_ds' => $request->line_ds,
                'line_dt' => $request->line_dt,
                'line_dn' => $request->line_dn,
                'tegangan_rn' => $request->tegangan_rn,
                'tegangan_sn' => $request->tegangan_sn,
                'tegangan_tn' => $request->tegangan_tn,
                'document'=> $document_name
            ]);

            // Get Last Data
            $last_data = Trafo::where('address_id', $request->address)->latest()->first();

            $line_ar = $request->line_ar != 0 ? $last_data->line_ar+$request->line_ar : $last_data->line_ar;
            $line_as = $request->line_as != 0 ? $last_data->line_as+$request->line_as : $last_data->line_as;
            $line_at = $request->line_at != 0 ? $last_data->line_at+$request->line_at : $last_data->line_at;
            $line_an = $request->line_an != 0 ? $last_data->line_an+$request->line_an : $last_data->line_an;

            $line_br = $request->line_br != 0 ? $last_data->line_br+$request->line_br : $last_data->line_br;
            $line_bs = $request->line_bs != 0 ? $last_data->line_bs+$request->line_bs : $last_data->line_bs;
            $line_bt = $request->line_bt != 0 ? $last_data->line_bt+$request->line_bt : $last_data->line_bt;
            $line_bn = $request->line_bn != 0 ? $last_data->line_bn+$request->line_bn : $last_data->line_bn;

            $line_cr = $request->line_cr != 0 ? $last_data->line_cr+$request->line_cr : $last_data->line_cr;
            $line_cs = $request->line_cs != 0 ? $last_data->line_cs+$request->line_cs : $last_data->line_cs;
            $line_ct = $request->line_ct != 0 ? $last_data->line_ct+$request->line_ct : $last_data->line_ct;
            $line_cn = $request->line_cn != 0 ? $last_data->line_cn+$request->line_cn : $last_data->line_cn;

            $line_dr = $request->line_dr != 0 ? $last_data->line_dr+$request->line_dr : $last_data->line_dr;
            $line_ds = $request->line_ds != 0 ? $last_data->line_ds+$request->line_ds : $last_data->line_ds;
            $line_dt = $request->line_dt != 0 ? $last_data->line_dt+$request->line_dt : $last_data->line_dt;
            $line_dn = $request->line_dn != 0 ? $last_data->line_dn+$request->line_dn : $last_data->line_dn;

            $line_ur = $line_ar+$line_br+$line_cr+$line_dr;
            $line_us = $line_as+$line_bs+$line_cs+$line_ds;
            $line_ut = $line_at+$line_bt+$line_ct+$line_dt;
            $line_un = $line_an+$line_bn+$line_cn+$line_dn;

            $tegangan_rn = $request->tegangan_rn != 0 ? $last_data->tegangan_rn+$request->tegangan_rn : $last_data->tegangan_rn;
            $tegangan_sn = $request->tegangan_sn != 0 ? $last_data->tegangan_sn+$request->tegangan_sn : $last_data->tegangan_sn;
            $tegangan_tn = $request->tegangan_tn != 0 ? $last_data->tegangan_tn+$request->tegangan_tn : $last_data->tegangan_tn;
            
            $maxv = max($line_ur,$line_us,$line_ut);
            $minv = min($line_ur,$line_us,$line_ut);

            // mencari delta
            if($minv == 0){
                $delta = 0;
            }else{
                $delta = (float)((($maxv-$minv))*100/$minv);
            }

            // mencari arus
            if($line_un > $minv){
                $arus = "ARUS NETRAL LEBIH BESAR";
            }else{
                $arus = "NORMAL";
            }

            $trafo = new Trafo();

            $trafo->address_id      = $request->address;
            $trafo->transaction_id  = $transaction->id;

            $trafo->line_ar     = $line_ar;
            $trafo->line_as     = $line_as;
            $trafo->line_at     = $line_at;
            $trafo->line_an     = $line_an;

            $trafo->line_br     = $line_br;
            $trafo->line_bs     = $line_bs;
            $trafo->line_bt     = $line_bt;
            $trafo->line_bn     = $line_bn;

            $trafo->line_cr     = $line_cr;
            $trafo->line_cs     = $line_cs;
            $trafo->line_ct     = $line_ct;
            $trafo->line_cn     = $line_cn;

            $trafo->line_dr     = $line_dr;
            $trafo->line_ds     = $line_ds;
            $trafo->line_dt     = $line_dt;
            $trafo->line_dn     = $line_dn;

            $trafo->line_ur     = $line_ur;
            $trafo->line_us     = $line_us;
            $trafo->line_ut     = $line_ut;
            $trafo->line_un     = $line_un;

            $trafo->tegangan_rn = $tegangan_rn;
            $trafo->tegangan_sn = $tegangan_sn;
            $trafo->tegangan_tn = $tegangan_tn;

            $trafo->delta       = round($delta, 2);
            $trafo->arus        = $arus;

            $trafo->document    = $document_name;

            if($trafo->save()){
                DB::commit();
            }else{
                DB::rollBack();
            }

            return redirect()->route('gardu.index')->withStatus('Gardu baru berhasil ditambahkan.');
        } catch (\Throwable $th) {
            DB::rollBack();
            return $th;
        }
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return view('gardu.show-by-gtt');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $trafo = Trafo::where('id', $id)->first();
        // $addresses = Address::latest()->get();

        // return view('gardu.edit', compact('trafo', 'addresses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'address'       => 'required|integer',
            
            'line_ar'       => 'required|integer',
            'line_as'       => 'required|integer',
            'line_at'       => 'required|integer',
            'line_an'       => 'required|integer',

            'line_br'       => 'required|integer',
            'line_bs'       => 'required|integer',
            'line_bt'       => 'required|integer',
            'line_bn'       => 'required|integer',

            'line_cr'       => 'required|integer',
            'line_cs'       => 'required|integer',
            'line_ct'       => 'required|integer',
            'line_cn'       => 'required|integer',

            'line_dr'       => 'required|integer',
            'line_ds'       => 'required|integer',
            'line_dt'       => 'required|integer',
            'line_dn'       => 'required|integer',

            'line_ur'       => 'required|integer',
            'line_us'       => 'required|integer',
            'line_ut'       => 'required|integer',
            'line_un'       => 'required|integer',

            'tegangan_rn'   => 'required|integer',
            'tegangan_sn'   => 'required|integer',
            'tegangan_tn'   => 'required|integer',
        ]);

        $maxv = max($request->line_ur,$request->line_us,$request->line_ut);
        $minv = min($request->line_ur,$request->line_us,$request->line_ut);

        // mencari delta
        if($minv == 0){
            $delta = 0;
        }else{
            $delta = (float)((($maxv-$minv))*100/$minv);
        }

        // mencari arus
        if($request->line_un > $minv){
            $arus = "ARUS NETRAL LEBIH BESAR";
        }else{
            $arus = "NORMAL";
        }

        $trafo = Trafo::find($id);

        $trafo->address_id  = $request->address;

        $trafo->line_ar     = $request->line_ar;
        $trafo->line_as     = $request->line_as;
        $trafo->line_at     = $request->line_at;
        $trafo->line_an     = $request->line_an;

        $trafo->line_br     = $request->line_br;
        $trafo->line_bs     = $request->line_bs;
        $trafo->line_bt     = $request->line_bt;
        $trafo->line_bn     = $request->line_bn;

        $trafo->line_cr     = $request->line_cr;
        $trafo->line_cs     = $request->line_cs;
        $trafo->line_ct     = $request->line_ct;
        $trafo->line_cn     = $request->line_cn;

        $trafo->line_dr     = $request->line_dr;
        $trafo->line_ds     = $request->line_ds;
        $trafo->line_dt     = $request->line_dt;
        $trafo->line_dn     = $request->line_dn;

        $trafo->line_ur     = $request->line_ur;
        $trafo->line_us     = $request->line_us;
        $trafo->line_ut     = $request->line_ut;
        $trafo->line_un     = $request->line_un;

        $trafo->tegangan_rn = $request->tegangan_rn;
        $trafo->tegangan_sn = $request->tegangan_sn;
        $trafo->tegangan_tn = $request->tegangan_tn;

        $trafo->delta       = $delta;
        $trafo->arus        = $arus;

        if ($request->file('document')) {
            $name = time().'-'.date('dmY').'.'.$request->document->clientExtension();
            Storage::disk('public')->put('documents/'.$name, File::get($request->document));
            $trafo->document = $name;
        }

        $trafo->save();

        return redirect()->route('gardu.index')->withStatus('Gardu baru berhasil diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');
        $address = Trafo::where('id', $id)->first();

        Trafo::where('id', $id)->delete();

        return redirect()->route('gardu.detail', $address->address_id)->withStatus('GTT berhasil dihapus.');
    }

    public function detail($id)
    {
        $trafos = Trafo::where('address_id', $id)->with('address')->latest()->get();
        $gardu = Address::where('id', $id)->first();
        $latest = Trafo::latest()->first()->id;

        return view('gardu.detail', compact('trafos', 'gardu', 'latest'));
    }

}
