<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate(['id'=>1],[
            'name'          => 'Administrator',
            'email'         => 'admin@me.com',
            'password'      => Hash::make('password'),
            'role'          => 'admin',
            'email_verified_at' => Carbon::now()
        ]);
    }
}
