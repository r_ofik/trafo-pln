<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trafos', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('transaction_id')->nullable();
            $table->unsignedBigInteger('address_id');
            $table->string('line_ar');
            $table->string('line_as');
            $table->string('line_at');
            $table->string('line_an');

            $table->string('line_br');
            $table->string('line_bs');
            $table->string('line_bt');
            $table->string('line_bn');

            $table->string('line_cr');
            $table->string('line_cs');
            $table->string('line_ct');
            $table->string('line_cn');

            $table->string('line_dr');
            $table->string('line_ds');
            $table->string('line_dt');
            $table->string('line_dn');

            $table->string('line_ur');
            $table->string('line_us');
            $table->string('line_ut');
            $table->string('line_un');

            $table->string('tegangan_rn');
            $table->string('tegangan_sn');
            $table->string('tegangan_tn');

            $table->string('delta');
            $table->string('arus');

            $table->string('no_tiang')->nullable();
            $table->string('document')->nullable();
            $table->timestamps();


            $table->foreign('address_id')->references('id')->on('addresses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trafos');
    }
};
