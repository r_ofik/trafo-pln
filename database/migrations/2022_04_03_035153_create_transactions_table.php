<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('line_ar');
            $table->string('line_as');
            $table->string('line_at');
            $table->string('line_an');

            $table->string('line_br');
            $table->string('line_bs');
            $table->string('line_bt');
            $table->string('line_bn');

            $table->string('line_cr');
            $table->string('line_cs');
            $table->string('line_ct');
            $table->string('line_cn');

            $table->string('line_dr');
            $table->string('line_ds');
            $table->string('line_dt');
            $table->string('line_dn');

            $table->string('line_ur')->nullable();
            $table->string('line_us')->nullable();
            $table->string('line_ut')->nullable();
            $table->string('line_un')->nullable();

            $table->string('tegangan_rn')->nullable();
            $table->string('tegangan_sn')->nullable();
            $table->string('tegangan_tn')->nullable();

            $table->string('delta')->nullable();
            $table->string('arus')->nullable();

            $table->string('document')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
};
