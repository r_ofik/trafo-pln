<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="{{asset('images/pln-logo.png')}}" type="image/x-icon">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'King Gatra') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        a{text-decoration: none}
    </style>
</head>
<body>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
        
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                    </li>
                </ul>
        
                <ul class="navbar-nav ml-auto">

                    <li class="nav-item">
                        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" role="button" href="{{ route('logout') }}" 
                            onclick="event.preventDefault(); 
                            document.getElementById('logout-form').submit();">
                            Logout <i class="fas fa-arrow-right"></i>
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>
                </ul>
            </nav>
        
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
            
                <a href="/" class="brand-link">
                    <img src="{{asset('images/pln-logo.png')}}" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                    <span class="brand-text font-weight-light">King Gatra</span>
                </a>
            
            <div class="sidebar">
            
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="{{asset('images/pln-logo.png')}}" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">{{Auth::user()->name}} <br> {{Auth::user()->role}}</a>
                </div>
            </div>
            
            
            <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                
                <li class="nav-item">
                    <a href="{{route('address.index')}}" class="nav-link {{(Request::route()->getName() == 'address.index' || Request::route()->getName() == 'address.create' || Request::route()->getName() == 'address.edit') ? 'active' : ''}}">
                        <i class="far fa-address-card nav-icon"></i>
                        <p>Alamat</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('gardu.index')}}" class="nav-link {{(Request::route()->getName() == 'gardu.index') ? 'active' : ''}}">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                        Gardu
                        </p>
                    </a>
                </li>
                @can('isAdmin')
                <li class="nav-item">
                    <a href="{{route('users.index')}}" class="nav-link {{(Request::route()->getName() == 'users.index' || Request::route()->getName() == 'users.create' || Request::route()->getName() == 'users.edit' || Request::route()->getName() == 'users.show') ? 'active' : ''}}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                        Pengguna
                        </p>
                    </a>
                </li>
                @endcan
            </ul>
            </nav>
            
            </div>
            
            </aside>
        
            <div class="content-wrapper">
            
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0">{{$title ?? 'Selamat Datang'}}</h1>
                            </div>
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                                    @if (isset($parent) && $parent != '')
                                        <li class="breadcrumb-item"><a href="{{route($parent_link)}}">{{$parent}}</a></li>
                                    @endif
                                    <li class="breadcrumb-item active">{{$child ?? 'Selamat Datang'}}</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            
            
                <div class="content">
                    <div class="container-fluid">
                        @yield('content')
                    </div>
                </div>
            
            </div>
        
        
            <footer class="main-footer">
            
                <div class="float-right d-none d-sm-inline">
                    Develop By Madura Islamic University College Students
                </div>
                
                <strong>Copyright &copy; {{date('Y')}} </strong> All rights reserved.
            </footer>
        </div>
    @yield('script')
</body>
</html>
