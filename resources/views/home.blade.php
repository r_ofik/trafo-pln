@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title">Selamat Datang {{Auth::user()->name}}</h5>
            </div>
            <div class="card-body text-center">
                Ini adalah aplikasi <strong>King Gatra</strong>
            </div>
        </div>
    </div>
</div>
@endsection
