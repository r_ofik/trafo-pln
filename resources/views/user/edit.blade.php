@extends('layouts.app', [
    'title'         => 'Edit Pengguna',
    'parent'        => 'Pengguna',
    'parent_link'   => 'users.index',
    'child'         => 'Edit Pengguna'
])

@section('content')
    <div class="row">
        <div class="col">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <div class="card-tools">
                        <a href="{{route('users.index')}}" class="btn btn-sm btn-outline-secondary"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                    <h5 class="card-title">Edit Data Pengguna</h5>
                </div>
                <div class="card-body">
                    <form action="{{route('users.update', $user->id)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="row mb-3">
                            <div class="col-md-6 col-sm-12">
                                <div class="form group">
                                    <label>Nama Lengkap <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Nama Lengkap" value="{{old('name', $user->name)}}">
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form group">
                                    <label>Email <span class="text-danger">*</span></label>
                                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email" value="{{old('email', $user->email)}}">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-6 col-sm-12">
                                <div class="form group">
                                    <label>Kata Sandi <span class="text-danger">*</span></label>
                                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Kata Sandi">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form group">
                                    <label>Ulangi Kata Sandi <span class="text-danger">*</span></label>
                                    <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" placeholder="Konfirmasi Kata Sandi">
                                    @error('password_confirmation')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-6 col-sm-12">
                                <div class="form group">
                                    <label>Peran <span class="text-danger">*</span></label>
                                    <select class="form-select @error('role') is-invalid @enderror" name="role">
                                        <option value="">Pilih Peran</option>
                                        <option value="admin" @if(old('role', $user->role) == 'admin') selected @endif>Admin</option>
                                        <option value="editor" @if(old('role', $user->role) == 'editor') selected @endif>Editor</option>
                                    </select>
                                    @error('role')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 pt-4">
                                <span class="text-warning">Note : Biarkan kata sandi kosong, jika tidak ingin mengubah kata sandi</span>
                            </div>
                        </div>

                        <hr>
                        <div class="row">
                            <div class="col">
                                <button type="submit" class="btn btn-outline-success"><i class="fa fa-save"></i> Simpan</button>
                                <a href="{{route('users.index')}}" class="btn btn-outline-secondary"><i class="fa fa-times"></i> Batal</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection