@extends('layouts.app', [
    'title'         => 'Pengguna',
    'parent'        => '',
    'parent_link'   => '',
    'child'         => 'Pengguna'
])

@section('content')
@if (session('status'))
    <div class="alert alert-info alert-dismissible fade show" role="alert">
        <strong>{{ session('status') }}.</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif
    <div class="row">
        <div class="col">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <div class="card-tools">
                        <a href="{{route('users.create')}}" class="btn btn-sm btn-outline-success"><i class="fa fa-plus"></i> Tambah</a>
                    </div>
                    <h5 class="card-title">
                        Data Pengguna
                    </h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover" id="data" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Peran</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- {{$users}} --}}
                                @foreach ($users as $key => $user)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->role }}</td>
                                        <td>
                                            <a href="{{route('users.edit', $user->id)}}" class="btn btn-sm btn-outline-warning">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <button data-bs-toggle="modal" data-bs-target="#delete" onclick="confirmdelete('{{$user->id}}','{{$user->name}}')" class="btn btn-sm btn-outline-danger">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Delete --}}
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" method="POST">
                    @csrf
                    @method('DELETE')
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">
                            Hapus <span class="name"></span>
                        </h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id">
                        <p>
                            Hapus <span class="text-success name"></span> ?
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal"> <i class="fa fa-times"></i> Batal</button>
                        <button type="submit" class="btn btn-outline-danger"> <i class="fa fa-trash"></i> Hapus</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
@endsection

@section('script')
    <script>
        setTimeout(function () {
            $(document).ready(function(){
                $('#data').dataTable();
            });
        }, 1000);

        function confirmdelete(id,name){
            $('#delete form').attr('action', '/users/'+id)
            $('#delete .name').html(name)
            $('#delete input[name=id]').val(id)
        }
    </script>
@endsection