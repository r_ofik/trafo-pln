@extends('layouts.app', [
    'title'         => 'Tambah Alamat',
    'parent'        => 'Alamat',
    'parent_link'   => 'address.index',
    'child'         => 'Tambah Alamat'
])

@section('content')
<div class="row">
    <div class="col">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <div class="card-tools">
                    <a href="{{route('address.index')}}" class="btn btn-sm btn-outline-secondary"><i class="fa fa-arrow-left"></i> Kembali</a>
                </div>
                <h5 class="card-title">Tambah Alamat Baru</h5>
            </div>
            <div class="card-body">
                <form action="{{route('address.update', $address->id)}}" method="post">
                    @csrf
                    @method('PUT')
                    <div class="row mb-3">
                        <div class="col-md-6 col-sm-12">
                            <div class="form group">
                                <label>Kode GTT <span class="text-danger">*</span></label>
                                <input type="text" class="form-control @error('code') is-invalid @enderror" name="code" placeholder="Kode GTT" value="{{old('code', $address->code)}}">
                                @error('code')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form group">
                                <label>Alamat <span class="text-danger">*</span></label>
                                <input type="text" class="form-control @error('address') is-invalid @enderror" name="address" placeholder="Alamat" value="{{old('address', $address->address)}}">
                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col">
                            <button type="submit" class="btn btn-outline-success"><i class="fa fa-save"></i> Simpan</button>
                            <a href="{{route('address.index')}}" class="btn btn-outline-secondary"><i class="fa fa-times"></i> Batal</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        setTimeout(function () {
        $(document).ready(function(){
            $('#gtt_code').select2({
                theme: "classic"
            });
        });
    }, 1000);
    </script>
@endsection