@extends('layouts.app', [
    'title'         => 'Gardu '.$gardu->code.'-'.$gardu->address,
    'parent'        => 'Gardu',
    'parent_link'   => 'gardu.index',
    'child'         => 'Gardu '.$gardu->code.'-'.$gardu->address
])

@section('content')
@if (session('status'))
    <div class="alert alert-info alert-dismissible fade show" role="alert">
        <strong>{{ session('status') }}.</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif

<div class="row">
    <div class="col">
        <div class="card card-primary card-outline">
            <div class="card-header">
                {{-- @can('isAdmin') --}}
                <div class="card-tools">
                    <a href="{{route('gardu.create', 1)}}" class="btn btn-sm btn-outline-success"><i class="fa fa-plus"></i> Tambah</a>
                </div>
                {{-- @endcan --}}
                <h5 class="card-title">
                    Data Gardu {{$gardu->code.'-'.$gardu->address}}
                </h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-striped" id="data" style="width:100%">
                        <thead class="bg-light">
                            <tr>
                                <th scope="col" class="text-center" rowspan="3">#</th>
                                <th scope="col" class="text-center" rowspan="3">Tgl Update</th>
                                <th scope="col" class="text-center" rowspan="3" class="bg-secondary">No GTT</th>
                                <th scope="col" class="text-center" rowspan="3">Alamat</th>
                                <th scope="col" class="text-center" colspan="20">Arus RMS (A)</th>
                                <th scope="col" class="text-center" rowspan="2" colspan="3" class="bg-info">Tegangan</th>
                                <th scope="col" class="text-center" colspan="2">Analisa Beban</th>
                                <th scope="col" class="text-center" rowspan="3">Dokumentasi</th>
                                <th scope="col" class="text-center" rowspan="3" >Aksi</th>      
                            <tr>
                                <th scope="col" class="text-center" colspan="4">LINE A</th>
                                <th scope="col" class="text-center" colspan="4">LINE B</th>
                                <th scope="col" class="text-center" colspan="4">LINE C</th>
                                <th scope="col" class="text-center" colspan="4">LINE D</th>
                                <th scope="col" class="text-center" colspan="4">UTAMA</th>
                                <th scope="col" class="text-center" rowspan="2">DELTA ARUS PHASA RMS MAX-MIN</th>
                                <th scope="col" class="text-center" rowspan="2">ARUS NETRAL DIBANDING <br>
                                 ARUS PHASE TERENDAH</th>
                    
                            </tr>
                            <tr>
                                <td class="bg-danger">R</td>
                                <td class="bg-warning">S</td>
                                <td class="bg-info">T</td>
                                <td class="bg-secondary">N</td>
                                <td class="bg-danger">R</td>
                                <td class="bg-warning">S</td>
                                <td class="bg-info">T</td>
                                <td class="bg-secondary">N</td>
                                <td class="bg-danger">R</td>
                                <td class="bg-warning">S</td>
                                <td class="bg-info">T</td>
                                <td class="bg-secondary">N</td>
                                <td class="bg-danger">R</td>
                                <td class="bg-warning">S</td>
                                <td class="bg-info">T</td>
                                <td class="bg-secondary">N</td>
                                <td class="bg-danger">R</td>
                                <td class="bg-warning">S</td>
                                <td class="bg-info">T</td>
                                <td class="bg-secondary">N</td>
                                <td class="bg-danger">R-N</td>
                                <td class="bg-warning">S-N</td>
                                <td class="bg-info">T-N</td>
                            </tr>                    
                        </thead>
                        <tbody>
                            @foreach ($trafos as $key => $trafo)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $trafo->updated_at->format('d-m-Y H:i:s') }}</td>
                                    <td>{{$trafo->address->code}}</td>
                                    <td>{{$trafo->address->address}}</td>
                                    <td class="bg-danger">{{$trafo->line_ar == 0 ? '-' : $trafo->line_ar}}</td>
                                    <td class="bg-warning">{{$trafo->line_as == 0 ? '-' : $trafo->line_as}}</td>
                                    <td class="bg-info">{{$trafo->line_at == 0 ? '-' : $trafo->line_at}}</td>
                                    <td class="bg-secondary">{{$trafo->line_an == 0 ? '-' : $trafo->line_an}}</td>
                                    <td class="bg-danger">{{$trafo->line_br == 0 ? '-' : $trafo->line_br}}</td>
                                    <td class="bg-warning">{{$trafo->line_bs == 0 ? '-' : $trafo->line_bs}}</td>
                                    <td class="bg-info">{{$trafo->line_bt == 0 ? '-' : $trafo->line_bt}}</td>
                                    <td class="bg-secondary">{{$trafo->line_bn == 0 ? '-' : $trafo->line_bn}}</td>
                                    <td class="bg-danger">{{$trafo->line_cr == 0 ? '-' : $trafo->line_cr}}</td>
                                    <td class="bg-warning">{{$trafo->line_cs == 0 ? '-' : $trafo->line_cs}}</td>
                                    <td class="bg-info">{{$trafo->line_ct == 0 ? '-' : $trafo->line_ct}}</td>
                                    <td class="bg-secondary">{{$trafo->line_cn == 0 ? '-' : $trafo->line_cn}}</td>
                                    <td class="bg-danger">{{$trafo->line_dr == 0 ? '-' : $trafo->line_dr}}</td>
                                    <td class="bg-warning">{{$trafo->line_ds == 0 ? '-' : $trafo->line_ds}}</td>
                                    <td class="bg-info">{{$trafo->line_dt == 0 ? '-' : $trafo->line_dt}}</td>
                                    <td class="bg-secondary">{{$trafo->line_dn == 0 ? '-' : $trafo->line_dn}}</td>
                                    <td class="bg-danger">{{$trafo->line_ur == 0 ? '-' : $trafo->line_ur}}</td>
                                    <td class="bg-warning">{{$trafo->line_us == 0 ? '-' : $trafo->line_us}}</td>
                                    <td class="bg-info">{{$trafo->line_ut == 0 ? '-' : $trafo->line_ut}}</td>
                                    <td class="bg-secondary">{{$trafo->line_un == 0 ? '-' : $trafo->line_un}}</td>
                                    <td class="bg-danger">{{$trafo->tegangan_rn == 0 ? '-' : $trafo->tegangan_rn}}</td>
                                    <td class="bg-warning">{{$trafo->tegangan_sn == 0 ? '-' : $trafo->tegangan_sn}}</td>
                                    <td class="bg-info">{{$trafo->tegangan_tn == 0 ? '-' : $trafo->tegangan_tn}}</td>
                                    <td>{{$trafo->delta}}</td>
                                    <td class="@if($trafo->arus == 'ARUS NETRAL LEBIH BESAR') bg-danger @endif">{{$trafo->arus}}</td>
                                    <td>
                                        @if ($trafo->document != null)
                                            <a href="{{asset('storage/documents/'.$trafo->document)}}" target="_blank" rel="noopener noreferrer">Document ({{$trafo->document}})</a>
                                        @endif
                                    </td>
                                    <td>
                                        {{-- <a href="{{route('gardu.edit', $trafo->id)}}" class="btn btn-sm btn-outline-warning">
                                            <i class="fa fa-edit"></i>
                                        </a> --}}
                                        @can('isAdmin')
                                        @if($latest == $trafo->id)
                                        <button data-bs-toggle="modal" data-bs-target="#delete" onclick="confirmdelete('{{$trafo->id}}','{{$trafo->address->address}}')" class="btn btn-sm btn-outline-danger">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        @endif
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Modal Delete --}}
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" method="POST">
                @csrf
                @method('DELETE')
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">
                        Hapus <span class="name"></span>
                    </h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id">
                    <p>
                        Hapus <span class="text-success name"></span> ?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal"> <i class="fa fa-times"></i> Batal</button>
                    <button type="submit" class="btn btn-outline-danger"> <i class="fa fa-trash"></i> Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    setTimeout(function () {
        $(document).ready(function(){
            $('#data').dataTable({
                "ordering": false,
                // "order": [[ 1, 'desc' ]]
            });
        });
    }, 1000);

    function confirmdelete(id,name){
        $('#delete form').attr('action', '/gardu/'+id)
        $('#delete .name').html(name)
        $('#delete input[name=id]').val(id)
    }
</script>
@endsection