@extends('layouts.app', [
    'title'         => 'Edit Data Gardu',
    'parent'        => 'Gardu',
    'parent_link'   => 'gardu.index',
    'child'         => 'Edit Data Gardu'
])

@section('content')
    <div class="row">
        <div class="col">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <div class="card-tools">
                        <a href="{{route('gardu.index')}}" class="btn btn-sm btn-outline-secondary"><i class="fa fa-arrow-left"></i> Kembali</a>
                    </div>
                    <h5 class="card-title">Edit Data Gardu</h5>
                </div>
                <div class="card-body">
                    <form action="{{route('gardu.update', $trafo->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="row mb-3">
                            <div class="col-md-6 col-sm-12">
                                <div class="form group">
                                    <label>Alamat <span class="text-danger">*</span></label>
                                    <select name="address" id="address" class="form-select @error('address') is-invalid @enderror" disabled>
                                        <option value="">Alamat</option>
                                        @foreach ($addresses as $address)
                                            <option value="{{$address->id}}" @if(old('address', $trafo->address_id) == $address->id) selected @endif>{{$address->code.' - '.$address->address}}</option>
                                        @endforeach
                                    </select>
                                    @error('address')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form group">
                                    <label>Line A <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <span class="input-group-text">R</span>
                                        <input type="number" class="form-control @error('line_ar') is-invalid @enderror" name="line_ar" placeholder="R" aria-label="R" value="{{old('line_ar', $trafo->line_ar)}}">
                                        <span class="input-group-text">S</span>
                                        <input type="number" class="form-control @error('line_as') is-invalid @enderror" name="line_as" placeholder="S" aria-label="S" value="{{old('line_as', $trafo->line_as)}}">
                                        <span class="input-group-text">T</span>
                                        <input type="number" class="form-control @error('line_at') is-invalid @enderror" name="line_at" placeholder="T" aria-label="T" value="{{old('line_at', $trafo->line_at)}}">
                                        <span class="input-group-text">N</span>
                                        <input type="number" class="form-control @error('line_an') is-invalid @enderror" name="line_an" placeholder="N" aria-label="N" value="{{old('line_an', $trafo->line_an)}}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-6 col-sm-12">
                                <div class="form group">
                                    <label>Line B <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <span class="input-group-text">R</span>
                                        <input type="number" class="form-control @error('line_br') is-invalid @enderror" name="line_br" placeholder="R" aria-label="R" value="{{old('line_br', $trafo->line_br)}}">
                                        <span class="input-group-text">S</span>
                                        <input type="number" class="form-control @error('line_bs') is-invalid @enderror" name="line_bs" placeholder="S" aria-label="S" value="{{old('line_bs', $trafo->line_bs)}}">
                                        <span class="input-group-text">T</span>
                                        <input type="number" class="form-control @error('line_bt') is-invalid @enderror" name="line_bt" placeholder="T" aria-label="T" value="{{old('line_bt', $trafo->line_bt)}}">
                                        <span class="input-group-text">N</span>
                                        <input type="number" class="form-control @error('line_bn') is-invalid @enderror" name="line_bn" placeholder="N" aria-label="N" value="{{old('line_bn', $trafo->line_bn)}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form group">
                                    <label>Line C <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <span class="input-group-text">R</span>
                                        <input type="number" class="form-control @error('line_cr') is-invalid @enderror" name="line_cr" placeholder="R" aria-label="R" value="{{old('line_cr', $trafo->line_cr)}}">
                                        <span class="input-group-text">S</span>
                                        <input type="number" class="form-control @error('line_cs') is-invalid @enderror" name="line_cs" placeholder="S" aria-label="S" value="{{old('line_cs', $trafo->line_cs)}}">
                                        <span class="input-group-text">T</span>
                                        <input type="number" class="form-control @error('line_ct') is-invalid @enderror" name="line_ct" placeholder="T" aria-label="T" value="{{old('line_ct', $trafo->line_ct)}}">
                                        <span class="input-group-text">N</span>
                                        <input type="number" class="form-control @error('line_cn') is-invalid @enderror" name="line_cn" placeholder="N" aria-label="N" value="{{old('line_cn', $trafo->line_cn)}}">
                                      </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-6 col-sm-12">
                                <div class="form group">
                                    <label>Line D <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <span class="input-group-text">R</span>
                                        <input type="number" class="form-control @error('line_dr') is-invalid @enderror" name="line_dr" placeholder="R" aria-label="R" value="{{old('line_dr', $trafo->line_dr)}}">
                                        <span class="input-group-text">S</span>
                                        <input type="number" class="form-control @error('line_ds') is-invalid @enderror" name="line_ds" placeholder="S" aria-label="S" value="{{old('line_ds', $trafo->line_ds)}}">
                                        <span class="input-group-text">T</span>
                                        <input type="number" class="form-control @error('line_dt') is-invalid @enderror" name="line_dt" placeholder="T" aria-label="T" value="{{old('line_dt', $trafo->line_dt)}}">
                                        <span class="input-group-text">N</span>
                                        <input type="number" class="form-control @error('line_dn') is-invalid @enderror" name="line_dn" placeholder="N" aria-label="N" value="{{old('line_dn', $trafo->line_dn)}}">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-12">
                                <div class="form group">
                                    <label>Utama <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <span class="input-group-text">R</span>
                                        <input type="number" class="form-control @error('line_ur') is-invalid @enderror" name="line_ur" placeholder="R" aria-label="R" value="{{old('line_ur', $trafo->line_ur)}}">
                                        <span class="input-group-text">S</span>
                                        <input type="number" class="form-control @error('line_us') is-invalid @enderror" name="line_us" placeholder="S" aria-label="S" value="{{old('line_us', $trafo->line_us)}}">
                                        <span class="input-group-text">T</span>
                                        <input type="number" class="form-control @error('line_ut') is-invalid @enderror" name="line_ut" placeholder="T" aria-label="T" value="{{old('line_ut', $trafo->line_ut)}}">
                                        <span class="input-group-text">N</span>
                                        <input type="number" class="form-control @error('line_un') is-invalid @enderror" name="line_un" placeholder="N" aria-label="N" value="{{old('line_un', $trafo->line_un)}}">
                                      </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-6 col-sm-12">
                                <div class="form group">
                                    <label>Tegangan <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <span class="input-group-text">R-N</span>
                                        <input type="number" class="form-control @error('tegangan_rn') is-invalid @enderror" name="tegangan_rn" placeholder="R-N" aria-label="R-N" value="{{old('tegangan_rn', $trafo->tegangan_rn)}}">
                                        <span class="input-group-text">S-N</span>
                                        <input type="number" class="form-control @error('tegangan_sn') is-invalid @enderror" name="tegangan_sn" placeholder="S-N" aria-label="S-N" value="{{old('tegangan_sn', $trafo->tegangan_sn)}}">
                                        <span class="input-group-text">T-N</span>
                                        <input type="number" class="form-control @error('tegangan_tn') is-invalid @enderror" name="tegangan_tn" placeholder="T-N" aria-label="T-N" value="{{old('tegangan_tn', $trafo->tegangan_tn)}}">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-12">
                                <div class="form group">
                                    <label>Document </label>
                                    {{-- <input type="file" name="document" id="document" class="form-control" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, application/pdf"> --}}
                                    <input type="file" name="document" id="document" class="form-control" accept="image/*">
                                </div>
                            </div>
                        </div>

                        <hr>
                        <div class="row">
                            <div class="col">
                                <button type="submit" class="btn btn-outline-success"><i class="fa fa-save"></i> Simpan</button>
                                <a href="{{route('gardu.index')}}" class="btn btn-outline-secondary"><i class="fa fa-times"></i> Batal</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- @section('script')
    <script>
        setTimeout(function () {
        $(document).ready(function(){
            $('#address').select2({
                theme: "classic"
            });
        });
    }, 1000);
    </script>
@endsection --}}