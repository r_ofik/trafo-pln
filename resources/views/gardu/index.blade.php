@extends('layouts.app', [
    'title'         => 'Gardu',
    'parent'        => '',
    'parent_link'   => '',
    'child'         => 'Gardu'
])

@section('content')
@if (session('status'))
    <div class="alert alert-info alert-dismissible fade show" role="alert">
        <strong>{{ session('status') }}.</strong>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif

<div class="row">
    <div class="col">
        <div class="card card-primary card-outline">
            <div class="card-header">
                {{-- @can('isAdmin') --}}
                <div class="card-tools">
                    <a href="{{route('gardu.create')}}" class="btn btn-sm btn-outline-success"><i class="fa fa-plus"></i> Tambah</a>
                </div>
                {{-- @endcan --}}
                <h5 class="card-title">
                    Data Gardu
                </h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-striped" id="data" style="width:100%">
                        <thead class="bg-light">
                            <tr>
                                <th scope="col" class="text-center align-middle" rowspan="3">#</th>
                                <th scope="col" class="text-center align-middle" rowspan="3">Tgl Update</th>
                                <th scope="col" class="text-center align-middle" rowspan="3" class="bg-secondary">No GTT</th>
                                <th scope="col" class="text-center align-middle" rowspan="3">Alamat</th>
                                <th scope="col" class="text-center align-middle" colspan="20">Arus RMS (A)</th>
                                <th scope="col" class="text-center align-middle" rowspan="2" colspan="3" class="bg-info">Tegangan</th>
                                <th scope="col" class="text-center align-middle" colspan="2">Analisa Beban</th>
                                <th scope="col" class="text-center align-middle" rowspan="3">Dokumentasi</th>
                                <th scope="col" class="text-center align-middle" rowspan="3" >Aksi</th>      
                            <tr>
                                <th scope="col" class="text-center align-middle" colspan="4">LINE A</th>
                                <th scope="col" class="text-center align-middle" colspan="4">LINE B</th>
                                <th scope="col" class="text-center align-middle" colspan="4">LINE C</th>
                                <th scope="col" class="text-center align-middle" colspan="4">LINE D</th>
                                <th scope="col" class="text-center align-middle" colspan="4">UTAMA</th>
                                <th scope="col" class="text-center align-middle" rowspan="2">DELTA ARUS PHASA RMS MAX-MIN</th>
                                <th scope="col" class="text-center align-middle" rowspan="2">ARUS NETRAL DIBANDING <br>
                                 ARUS PHASE TERENDAH</th>
                    
                            </tr>
                            <tr>
                                <td class="bg-danger">R</td>
                                <td class="bg-warning">S</td>
                                <td class="bg-info">T</td>
                                <td class="bg-secondary">N</td>
                                <td class="bg-danger">R</td>
                                <td class="bg-warning">S</td>
                                <td class="bg-info">T</td>
                                <td class="bg-secondary">N</td>
                                <td class="bg-danger">R</td>
                                <td class="bg-warning">S</td>
                                <td class="bg-info">T</td>
                                <td class="bg-secondary">N</td>
                                <td class="bg-danger">R</td>
                                <td class="bg-warning">S</td>
                                <td class="bg-info">T</td>
                                <td class="bg-secondary">N</td>
                                <td class="bg-danger">R</td>
                                <td class="bg-warning">S</td>
                                <td class="bg-info">T</td>
                                <td class="bg-secondary">N</td>
                                <td class="bg-danger">R-N</td>
                                <td class="bg-warning">S-N</td>
                                <td class="bg-info">T-N</td>
                            </tr>                    
                        </thead>
                        <tbody>
                            @foreach ($trafos as $key => $trafo)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $trafo->updated_at->format('d-m-Y H:i:s') }}</td>
                                    <td><a href="{{route('gardu.detail', $trafo->id)}}">{{$trafo->code}}</a></td>
                                    <td><a href="{{route('gardu.detail', $trafo->id)}}">{{$trafo->address}}</a></td>
                                    <td class="bg-danger">{{$trafo->gardu->line_ar == 0 ? '-' : $trafo->gardu->line_ar}}</td>
                                    <td class="bg-warning">{{$trafo->gardu->line_as == 0 ? '-' : $trafo->gardu->line_as}}</td>
                                    <td class="bg-info">{{$trafo->gardu->line_at == 0 ? '-' : $trafo->gardu->line_at}}</td>
                                    <td class="bg-secondary">{{$trafo->gardu->line_an == 0 ? '-' : $trafo->gardu->line_an}}</td>
                                    <td class="bg-danger">{{$trafo->gardu->line_br == 0 ? '-' : $trafo->gardu->line_br}}</td>
                                    <td class="bg-warning">{{$trafo->gardu->line_bs == 0 ? '-' : $trafo->gardu->line_bs}}</td>
                                    <td class="bg-info">{{$trafo->gardu->line_bt == 0 ? '-' : $trafo->gardu->line_bt}}</td>
                                    <td class="bg-secondary">{{$trafo->gardu->line_bn == 0 ? '-' : $trafo->gardu->line_bn}}</td>
                                    <td class="bg-danger">{{$trafo->gardu->line_cr == 0 ? '-' : $trafo->gardu->line_cr}}</td>
                                    <td class="bg-warning">{{$trafo->gardu->line_cs == 0 ? '-' : $trafo->gardu->line_cs}}</td>
                                    <td class="bg-info">{{$trafo->gardu->line_ct == 0 ? '-' : $trafo->gardu->line_ct}}</td>
                                    <td class="bg-secondary">{{$trafo->gardu->line_cn == 0 ? '-' : $trafo->gardu->line_cn}}</td>
                                    <td class="bg-danger">{{$trafo->gardu->line_dr == 0 ? '-' : $trafo->gardu->line_dr}}</td>
                                    <td class="bg-warning">{{$trafo->gardu->line_ds == 0 ? '-' : $trafo->gardu->line_ds}}</td>
                                    <td class="bg-info">{{$trafo->gardu->line_dt == 0 ? '-' : $trafo->gardu->line_dt}}</td>
                                    <td class="bg-secondary">{{$trafo->gardu->line_dn == 0 ? '-' : $trafo->gardu->line_dn}}</td>
                                    <td class="bg-danger">{{$trafo->gardu->line_ur == 0 ? '-' : $trafo->gardu->line_ur}}</td>
                                    <td class="bg-warning">{{$trafo->gardu->line_us == 0 ? '-' : $trafo->gardu->line_us}}</td>
                                    <td class="bg-info">{{$trafo->gardu->line_ut == 0 ? '-' : $trafo->gardu->line_ut}}</td>
                                    <td class="bg-secondary">{{$trafo->gardu->line_un == 0 ? '-' : $trafo->gardu->line_un}}</td>
                                    <td class="bg-danger">{{$trafo->gardu->tegangan_rn == 0 ? '-' : $trafo->gardu->tegangan_rn}}</td>
                                    <td class="bg-warning">{{$trafo->gardu->tegangan_sn == 0 ? '-' : $trafo->gardu->tegangan_sn}}</td>
                                    <td class="bg-info">{{$trafo->gardu->tegangan_tn == 0 ? '-' : $trafo->gardu->tegangan_tn}}</td>
                                    <td>{{$trafo->gardu->delta}}</td>
                                    <td class="@if($trafo->gardu->arus == 'ARUS NETRAL LEBIH BESAR') bg-danger @endif">{{$trafo->gardu->arus}}</td>
                                    <td>
                                        @if ($trafo->gardu->document != null)
                                            <a href="{{asset('storage/documents/'.$trafo->gardu->document)}}" target="_blank" rel="noopener noreferrer">Document ({{$trafo->gardu->document}})</a>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route('gardu.detail', $trafo->id)}}" class="btn btn-sm btn-outline-info">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Modal Delete --}}
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" method="POST">
                @csrf
                @method('DELETE')
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">
                        Hapus <span class="name"></span>
                    </h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id">
                    <p>
                        Hapus <span class="text-success name"></span> ?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal"> <i class="fa fa-times"></i> Batal</button>
                    <button type="submit" class="btn btn-outline-danger"> <i class="fa fa-trash"></i> Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    setTimeout(function () {
        $(document).ready(function(){
            $('#data').dataTable({
                "ordering": false,
                // "order": [[ 1, 'desc' ]]
            });
        });
    }, 1000);

    function confirmdelete(id,name){
        $('#delete form').attr('action', '/gardu/'+id)
        $('#delete .name').html(name)
        $('#delete input[name=id]').val(id)
    }
</script>
@endsection